# Installation Instructions

To run the PartyPlayer application you need a local installation of docker and docker-compose.
Installation Instructions:
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install)

# Required Third Party
**You can skip this step if you just want to see if the application runs.**

If you want to use the spotify component of our app you have to create a developer account at Spotify.

- create an app on [Spotify](https://developer.spotify.com/dashboard/)
- Add redirect uris in the settings
    - localhost:81/account/addcredentials/spotify
    - http://localhost:81/account/addcredentials/spotify
- Change the environment variables for sportify.clientId, sportify.secret, sportify.uri in `src/resources/application.properties`

## Run the application
- Create a new folder
- Clone the [frontend](https://gitlab.com/kzynn/partyplayer-ui) and the [backend](https://gitlab.com/kzynn/partyplayer) into this folder
- Copy the [docker-compose](docker-compose.yml) file in the folder
- Run `docker-compose up` (Sometimes the backend starts during the startup process of the db, if this happens and the command fails, just rerun it)
- Open your browser at [localhost:81](http://localhost:81)
