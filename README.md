<div align="center"><img src="https://gitlab.com/kzynn/partyplayer-doc/raw/master/logos/logo_black.png" alt="PartyPlayer Logo" /></div>


# PartyPlayer Documentation

This is the repository for the documentation of the PartyPlayer project.

- [SAD](sad.md)
- [SRS](SRS.md)
- [Testplan](testplan.md)
- [API-Definition](swagger-generate.yml)
- [Installation Intructions](installation.md)
