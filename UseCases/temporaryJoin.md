# PartyPlayer
## Use Case Specification: Temporary Join

# 1. Temporary Join
## 1.1 Brief Description
Upon joining an event as guest (via the invitation link), likes and dislikes of a song need to be connected to a unique user. This is needed to ensure proper like and dislike functionality.
For this the same technology (JWT) is used, as already used for identifying logged in users.
# 2. Flow of Events
## 2.1 Basic Flow
First the user needs to follow the public invitation link. Upon calling this link the first time, a temporary user session gets created and included in a new JWT. This JWT needs to be stored and sent with every like/dislike request.
Now a like can be connected to a temporary user. This allows a functionality like weighted votes.
### 2.1.1 Activity Diagram
![Activity diagram](img/temporaryJoin/temporaryJoin.png)
# 3. Special Requirements

## 3.1 First Special Requirement

# 4. Preconditions

## 4.1 Precondition One

A valid invitation link for an event is needed. Only with this link, the guest can request a JWT for further requests.

# 7. Function Points
We use the [TINY Tools FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html#TopPage) to calculate the function points for our use cases.
![FP](img/temporaryJoin/Unbenannt.PNG)
> Total of `13.65` FP 