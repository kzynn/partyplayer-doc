# PartyPlayer
## Use Case Specification: Delete Playlist

# Table of Contents
1. [Delete Playlist ](#1-use-case-name)  
1.1 [Brief Description ](#11-brief-description)  
2. [Flow of Events ](#2-flow-of-events)  
2.1 [Basic Flow ](#21-basic-flow)  
3. [Special Requirements ](#3-special-requirements)  
4. [Preconditions ](#4-preconditions)  
4.1 [Playlist created ](#41-Playlist-created)  
5. [Postconditions ](#5-postconditions)  
5.1 [Postcondition One ](#51-postcondition-one)  
6. [Extension Points ](#6-extension-points)  
6.1 [Name of Extension Point ](#61-name-of-extension-point)    


# Use-Case Specification: Delete Playlist

# 1. Delete Playlist

## 1.1 Brief Description

This usecase describes how to delete a playlist.

# 2. Flow of Events

## 2.1 Basic Flow
- user choose to delete the playlist
- a dialog comes up 
- user has the option to export the playlist as txt, or create a private Spotify Playlist
- user can share playlist to all members
- delete playlist

<img src="img/deletePlaylist/flow.png" alt="Activity Diagram">
<br>
<img src="img/deletePlaylist/popup.png" alt="popup mockup">
<img src="img/deletePlaylist/share.png" alt="share mockup">
<img src="img/deletePlaylist/export.png" alt="export mockup">

# 3. Special Requirements

- The user accept to get playlists. 
- The user has set the credentials to the platfrom
- The user is owner of the playlist

# 4. Preconditions

## 4.1 Playlist created
The user has created a playlist.

# 5. Postconditions
(n/a)

# 6. Extension Points

## 6.1 Options to export
For songs from other platforms than spotify we may implement an option to export the link e.g. youtube video.

# 7. Function Points
We use the [TINY Tools FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html#TopPage) to calculate the function points for our use cases.
![FP](img/deletePlaylist/fp.png)
> Total of 17.55 FP
