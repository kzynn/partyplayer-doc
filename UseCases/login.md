# PartyPlayer
## Use Case Specification: Login

# Table of Contents
1. [Login](#1-login)
    1. [Brief Description ](#11-brief-description)
2. [Flow of Events ](#2-flow-of-events)
    1. [Basic Flow ](#21-basic-flow)
        1. [Activity diagram](#211-activity-diagram)
        2. [Mockup](#212-mockup)
    2. [Alternative Flows](#22-alternative-flows)
3. [Special Requirements ](#3-special-requirements)
    1. [First Special Requirement ](#31-first-special-requirement)
4. [Preconditions ](#4-preconditions)
    1. [Precondition One ](#41-precondition-one)
5. [Postconditions ](#5-postconditions)
    1. [Postcondition One ](#51-postcondition-one)
6. [Extension Points ](#6-extension-points)
    1. [Name of Extension Point ](#61-name-of-extension-point)  


# 1. Login

## 1.1 Brief Description

This use case allows registered users to login with their username or email and password.

# 2. Flow of Events

## 2.1 Basic Flow

- The user is on the login page
- enters username/email
- enters password
- clicks the "login" button 
- user is beeing redirected


### 2.1.1 Activity diagram

![Activity diagram](img/login/ActivityDiagram.png)

### 2.1.2 Mockup

![Mockup](img/login/Mockup_login.png)

![Mockup](img/login/Mockup_login_error.png)

## 2.2 Alternative Flows
(n/a)

# 3. Special Requirements

## 3.1 Registered account

The user needs a registered account.

# 4. Preconditions

(n/a)

# 5. Postconditions

## 5.1 Use Application

The user is now able to use the PartyPlayer application. He can now create or join events or participate to an existing one.

# 6. Extension Points

(n/a)

# 7. Function Points
We use the [TINY Tools FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html#TopPage) to calculate the function points for our use cases.
![FP](img/login/fp.png)
> Total of 14.95 FP