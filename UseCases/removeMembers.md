# PartyPlayer
## Use Case Specification: Remove Members

# 1. Remove Members
## 1.1 Brief Description
If some members don't behave well, the admin of the event is able to remove the person from the event.


# 2. Flow of Events
## 2.1 Basic Flow
### 2.1.1 Activity Diagram
![Activity diagram](img/removeMembers/activityDiagram.png)
### 2.1.2 Mockups
![Mockup](img/removeMembers/mockup1.png)
![Mockup](img/removeMembers/mockup2.png)
![Mockup](img/removeMembers/mockup3.png)

# 3. Special Requirements
## 3.1 Be admin of the event
The person who wants to remove another user from an event has to be the one that created the event and is the admin of the event.

# 4. Preconditions
## 4.1 Be logged in
In order to remove users from an event, you have to be logged into the application.

# 5. Postconditions
## 5.1 User can no longer access the event
The user that was removed from the event by the admin is no longer able to access the event.

# 6. Extension Points
(n/a)

# 7. Function Points
We use the [TINY Tools FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html#TopPage) to calculate the function points for our use cases.
![FP](img/removeMembers/fp.png)
> Total of 21.45 FP
