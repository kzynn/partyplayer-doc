# PartyPlayer
## Use Case Specification: Use-Case Name

[Note: The following template is provided for use with the Rational Unified Process. Text enclosed in square brackets and displayed in blue italics (style=InfoBlue) is included to provide guidance to the author and should be deleted before publishing the document. A paragraph entered following this style will automatically be set to normal (style=Body Text).]

# Table of Contents
1. [Delete Event ](#1-use-case-name)
1.1 [Brief Description ](#11-brief-description)
2. [Flow of Events ](#2-flow-of-events)
2.1 [Basic Flow ](#21-basic-flow)
2.2 [Alternative Flows ](#22-alternative-flows)
2.2.1 [First Alternative Flow ](#221-first-alternative-flow)
2.2.1.1 [An Alternative Sub-flow ](#2211-an-alternative-sub-flow)
2.2.2 [Second Alternative Flow ](#222-second-alternative-flow)
3. [Special Requirements ](#3-special-requirements)
3.1 [First Special Requirement ](#31-first-special-requirement)
4. [Preconditions ](#4-preconditions)
4.1 [Precondition One ](#41-precondition-one)
5. [Postconditions ](#5-postconditions)
5.1 [Postcondition One ](#51-postcondition-one)
6. [Extension Points ](#6-extension-points)
6.1 [Name of Extension Point ](#61-name-of-extension-point)  
7. [Function Points](#7-function-points)


# Use-Case Specification: Delete Event

# 1. Event

## 1.1 Brief Description

This usecase describes how to delete a event.

# 2. Flow of Events

## 2.1 Basic Flow

- user navigate to list of all events their have created
- choose event to delete
- confirm delete

<img src="img/deleteEvent/flow.png" alt="flow chart">

# 3. Special Requirements

- the user is owner of the event



# 4. Preconditions

- the user created an event

## 4.1 Precondition One

# 5. Postconditions

(n/a)

## 5.1 Postcondition One

# 6. Extension Points
(n/a)

# 7. Function Points
We use the [TINY Tools FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html#TopPage) to calculate the function points for our use cases.

<img src="img/deleteEvent/fp.png" alt="flow chart">

> 20,15
