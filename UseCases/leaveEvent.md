# PartyPlayer
## Use Case Specification: Leave Event

# 1. Leave event
## 1.1 Brief Description
If the user wants to no longer be part of an event, he can leave the event.

# 2. Flow of Events
## 2.1 Basic Flow
### 2.1.1 Activity Diagram
![Activity diagram](img/leaveEvent/ActivityDiagram.png)
### 2.1.2 Mockups
![Mockup](img/leaveEvent/mockup1.png)
![Mockup](img/leaveEvent/mockup2.png)

# 3. Special Requirements
(n/a)

# 4. Preconditions
## 4.1 Be part of event
In order to leave an event, the user has to be part of the event he wants to leave.

# 5. Postconditions
## 5.1 Not longer member of event
If the user has left an event he is no longer able to access data of the event like viewing the playlist or voting songs.

# 6. Extension Points
(n/a)

# 7. Function Points
We use the [TINY Tools FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html#TopPage) to calculate the function points for our use cases.
![FP](img/leaveEvent/fp.png)
> Total of 19,5 FP