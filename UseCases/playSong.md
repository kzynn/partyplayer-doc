# PartyPlayer
## Use Case Specification: Play Song

[Note: The following template is provided for use with the Rational Unified Process. Text enclosed in square brackets and displayed in blue italics (style=InfoBlue) is included to provide guidance to the author and should be deleted before publishing the document. A paragraph entered following this style will automatically be set to normal (style=Body Text).]

# Table of Contents
1. [Play Song ](#1-play-song)
1.1 [Brief Description ](#11-brief-description)
2. [Flow of Events ](#2-flow-of-events)
2.1 [Basic Flow ](#21-basic-flow) 
2.1.1 [Activity Diagram ](#211-activity-diagram) 
2.2 [Alternative Flows ](#22-alternative-flows)
3. [Special Requirements ](#3-special-requirements)
4. [Preconditions ](#4-preconditions)
5. [Postconditions ](#5-postconditions)
6. [Extension Points ](#6-extension-points)
7. [Function Points](#7-function-points)


# Use-Case Specification: Play Song

# 1. Play Song

## 1.1 Brief Description

This UC allows the owner of an event to play the song at the top of the events playlist.

# 2. Flow of Events

## 2.1 Basic Flow

- User navigates to event page
- User clicks "Start playing"
- Song at the top of playlist starts playing 

### 2.1.1 Activity Diagram

![Activity Diagram](img/playSong/activityDiagram.png)

## 2.2 Alternative Flows

(n/a)

# 3. Special Requirements

(n/a)

# 5. Postconditions

(n/a)

# 6. Extension Points

(n/a)

# 7. Function Points
We use the [TINY Tools FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html#TopPage) to calculate the function points for our use cases.

![FP](img/playSong/fp.png)
> Total of 20.80 FP