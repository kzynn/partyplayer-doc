# PartyPlayer
## Use Case Specification: Create new Account
# Table of Contents
1. [Create new Account ](#1-create-new-account)
    1.1 [Brief Description ](#11-brief-description)
2. [Flow of Events ](#2-flow-of-events)
    2.1 [Basic Flow ](#21-basic-flow)
    2.2 [Mockups ](#22-mockup)
    2.3 [Feature File](#23-feature-file)
3. [Special Requirements ](#3-special-requirements)
    3.1 [Valid Email Address ](#31-valid-email-address)
4. [Preconditions ](#4-preconditions)
    4.1 [Internet ](#41-internet)
4.2 [Email Address ](#42-email-address)
5. [Postconditions ](#5-postconditions)
    5.1 [Postcondition One ](#51-postcondition-one)

# Use-Case Name: Create new Account

# 1. Create new Account

## 1.1 Brief Description

This Use-Case allow the user to create an account for the PartyPlayer-Web-Application. With this Account the user is able
to create and manage playlists as administrator. The account is a important requirement for our project. 

# 2. Flow of Events

## 2.1 Basic Flow

If the user wants to create an Account on our website, all he has to do is to fill the registration form. The information
gathered by this form are name, email and password (including password repeat). With this information the user can create
the account, but it's still locked. In order to unlock the account, a verification link is sent to the specified email adress.
By clicking this link, the account is now unlocked and ready to use.

## 2.2 Mockup

![Mockup Register Form](img/registerAccount/RegisterForm.png)
![Mockup Verify](img/registerAccount/Verify_Account_Email.png)

## 2.3 Feature File
[Feature File](https://gitlab.com/kzynn/partyplayer-ui/-/blob/master/features/Register.feature)

# 3. Special Requirements

## 3.1 Valid Email Address

Of course in order to activate and verify the created account, you need a valid email address. This
is because a mail gets sent to the specified address, in order to verify the ownership and prevent bot abuse.

# 4. Preconditions

## 4.1 Internet

Having a stable internet connection is key. Otherwise none of the said tasks can be done.

## 4.2 Email Address

An email account, that you have access to and is not already in use on this website.

# 5. Postconditions

## 5.1 Postcondition One

The user is not yet verified and can't log in.

## 5.1 Postcondition Two

The user has already verified his account by clicking the link in the sent mail. He is now
able to normally user his account with all features.

# 7. Function Points
We use the [TINY Tools FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html#TopPage) to calculate the function points for our use cases.
![FP](img/registerAccount/fp.png)
> Total of 21.45 FP