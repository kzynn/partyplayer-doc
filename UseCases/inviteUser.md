# PartyPlayer
## Use Case Specification: Invite User to Playlist
# Table of Contents
1. [Invite User to Playlist ](#1-invite-user-to-playlist)
    1.1 [Brief Description ](#11-brief-description)
2. [Flow of Events ](#2-flow-of-events)
    2.1 [Basic Flow ](#21-basic-flow)
    2.2 [Mockups ](#22-mockup)
    2.3 [Feature Files](#23-feature-files)
3. [Special Requirements ](#3-special-requirements)
    3.1 [Existing Member ](#31-existing-user)
4. [Preconditions ](#4-preconditions)
    4.1 [Account ](#41-account)
    4.2 [Playlist ](#42-playlist)
5. [Postconditions ](#5-postconditions)
    5.1 [Accept invitation ](#51-accept-invitation)

# Use-Case Name: Create new Account

# 1. Invite User to Playlist

## 1.1 Brief Description
This Use-Case allows the user and owner of a playlist to invite a specific user to the playlist. With this invitation 
the invited user is capable of adding new songs and voting for existing songs within this playlist.

# 2. Flow of Events

## 2.1 Basic Flow

The user first needs to create a playlist or open an existing playlist that he created. Within this playlist the user 
can find a "Invite User" button, which opens a corresponding form. In this form the user needs to submit the email address
of the user he wants to invite. With that being done the systems sends an invitation to the specified user. All this said 
user needs to do is to click on accept. After that, the user gets the necessary permissions to take part in the playlists
voting and song proposing purpose.

## 2.2 Mockup

![Mockup Popup](img/inviteUser/inviteUserPopup.png)
![Mockup Invitation_Email](img/inviteUser/invitationEmail.png)

## 2.3 Feature Files
[Feature File](https://gitlab.com/kzynn/partyplayer-ui/-/blob/master/features/AddMember.feature)

# 3. Special Requirements

## 3.1 Existing user

If the user wants to invite another user, this user needs to exist with the stated email. This is due to the way the invitation
is being sent. It is sent via email. Its important to know, that in order to respect the privacy of our users, there is 
no autocomplete function within this input field. This means, that the user doesn't get an error message, if the email 
he specified within the form does not correspond to an existing account.

# 4. Preconditions

## 4.1 Account

Of course a valid and active account is needed, to create a playlist.

## 4.2 Playlist

A playlist needs to be created or an existing playlist needs to be opened in order to add a user to it. This also only works,
if the user that wants to sent the invitation is also the creator of this said playlist.

# 5. Postconditions

## 5.1 Accept invitation

The invited user gets an email, in which he can accept the invitation, by clicking on the "Accept" button.
