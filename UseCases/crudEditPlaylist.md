# PartyPlayer
## Use Case Specification: Edit Playlist CRUD

# Table of Contents

1. [Edit Playlist CRUD ](#1-edit-playlist-crud)
1.1 [Brief Description ](#11-brief-description)
2. [Flow of Events ](#2-flow-of-events)
2.1 [Basic Flow ](#21-basic-flow)
2.1.1 [Activity Diagram ](#211-activity-diagram)
2.1.2 [Mockup ](#212-mockup)
3. [Special Requirements ](#3-special-requirements)
3.1 [Owning account or having invitation link ](#31-owning-account-or-having-invitation-link)
4. [Preconditions ](#4-preconditions)
4.1 [Invitation Link ](#41-invitation-link)
4.2 [Be logged-in ](#42-be-logged-in)
5. [Postconditions ](#5-postconditions)
5.1 [Add song ](#51-add-song)
5.2 [Vote song ](#52-vote-song)
5.3 [Delete song ](#53-delete-song)
5.4 [View songs in playlist ](#54-view-songs-in-playlist)
6. [Extension Points ](#6-extension-points)


# 1. Edit Playlist CRUD

## 1.1 Brief Description

This use case allows users to view the songs in the playlist, add new songs to it and vote them. In additional it allows the group admin to remove songs from the playlist.

# 2. Flow of Events

## 2.1 Basic Flow

### 2.1.1 Activity Diagram

<img src="img/crudEditPlaylist/activityDiagram.png" alt="Activity Diagram">

[Feature File](https://gitlab.com/kzynn/partyplayer-ui/-/blob/master/features/VoteSong.feature)

### 2.1.2 Mockup

<img src="img/crudEditPlaylist/mockup1.png" alt="Activity Diagram">
<br>
<img src="img/crudEditPlaylist/mockup2.png" alt="Activity Diagram">
<br>
<img src="img/crudEditPlaylist/mockup3.png" alt="Activity Diagram">

# 3. Special Requirements

## 3.1 Owning account or having invitation link

The user needs to have access to the group to view its playlist and edit it, so he has to be logged in with his account, which has access to the playlist of the group or needs to have an invitation link to the group.

# 4. Preconditions

## 4.1 Invitation Link

For adding, voting and viewing songs in the playlist it is enough to have an invitation link.

## 4.2 Be logged-in

Only the group owner, who is logged in with his account, is able to delete songs in the playlist.

# 5. Postconditions

## 5.1 Add song

The song is added to the playlist and can be seen and voted by other users. The song is automatically voted up by the person who added it.

## 5.2 Vote song

The song has one vote more or less, depending on up or downvote and is displayed with updated vote count to the others. The song is also rearranged in the view regarding his new vote count.

## 5.3 Delete song

The song is removed from the playlist. Nobody can see or vote it anymore.

## 5.4 View songs in playlist

# 6. Extension Points
(n/a)

# 7. Function Points
We use the [TINY Tools FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html#TopPage) to calculate the function points for our use cases.
![FP](img/crudEditPlaylist/fp.png)
> Total of 42.25 FP
