# PartyPlayer
## Use Case Specification: Add Credentials for Music Platform

1. [Add Credentials for Music Platform ](#1-add-credentials-for-music-platform)
    1.1 [Brief Description ](#11-brief-description)
2. [Flow of Events ](#2-flow-of-events)
    2.1 [Basic Flow ](#21-basic-flow)
        2.1.1 [Activity Diagram ](#211-activity-diagram)
        2.1.2 [Mockup ](#212-mockup)
    2.2 [Alternative Flows ](#22-alternative-flows)
3. [Special Requirements ](#3-special-requirements)
    3.1 [Own Account on Music Platform ](#31-own-account-on-music-platform)
4. [Preconditions ](#4-preconditions)
    4.1 [Log-in ](#41-log-in)
5. [Postconditions ](#5-postconditions)
    5.1 [Play music from platform ](#51-play-music-from-platform)
6. [Extension Points ](#6-extension-points)


# 1. Add Credentials for Music Platform

## 1.1 Brief Description

This use case allows the user to add credentials for music platforms from which songs can be added to the playlist and played. The user can select for which platforms he wants to enter credentials and then has to fill his username or email and his password.

# 2. Flow of Events

## 2.1 Basic Flow

- user is in profile view
- clicks on "add credentials"
- selcts platform for which he wants to add credentials 
- enters his credentials for the chosen platform
- he is now able to access music from this platform

[Feature File](https://gitlab.com/kzynn/partyplayer-ui/-/blob/master/features/AddCredentialsForSpotify.feature)


### 2.1.1 Activity Diagram

<img src="img/addCredentials/activityDiagram.png" alt="Activity Diagram">

### 2.1.2 Mockup

<img src="img/addCredentials/mockup1.png" alt="Activity Diagram">
<br>
<img src="img/addCredentials/mockup2.png" alt="Activity Diagram">

## 2.2 Alternative Flows

(n/a)

# 3. Special Requirements

## 3.1 Own Account on Music Platform

The user has to own an account on the music platform he wants to add, if one is required to get access to songs.

# 4. Preconditions

## 4.1 Log-in

The user has to be logged with his PartyPlayer account to add credentials for a music platforms.

# 5. Postconditions

## 5.1 Play music from platform

The user is now able to add songs from the added platform to his playlist and play them.

# 6. Extension Points

(n/a)

# 7. Function Points
We use the [TINY Tools FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html#TopPage) to calculate the function points for our use cases.
![FP](img/addCredentials/fp.png)
> Total of 29.25 FP
