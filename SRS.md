<div align="center"><img src="https://gitlab.com/kzynn/partyplayer-doc/raw/master/logos/logo_black.png" alt="PartyPlayer Logo" /></div>

# **PartyPlayer** <br> Software Requirements Specification

# Table of Contents

<!-- toc -->
1. [Introduction](#1-introduction)
	1. [Purpose](#11-purpose)
	2. [Scope](#12-scope)
	3. [Definitions, Acronyms and Abbreviations](#13-definitions,-acronyms-and-abbreviations)
	4. [References](#14-references)
	5. [Overview](#15-overview)
2. [Overall Description](#2-overall-description)
3. [Specific Requirements](#3-specific-requirements)
	1. [Functionality](#31-functionality)
		1. [Account](#311-account)
			1. [Create account](#3111-create-account)
			2. [Login](#3112-login)
			3. [Logout](#3113-logout)
			4. [Add credentials for music platforms](#3114-add-credentials-for-music-platforms)
			5. [Delete account](#3115-delete-account)
			6. [Edit account](#3116-edit-account)
			7. [Confirm email](#3117-confirm-email)
			8. [Reset Password](#3118-reset-password)
		2. [Event](#312-event)
			1. [Create](#3121-create)
			2. [Add member](#3122-add-members)
			3. [Remove member](#3123-remove-members)
			4. [Leave](#3124-leave)
			5. [Delete](#3125-delete)
			6. [Rename](#3126-rename)
			7. [Show most frequently played songs](#3127-show-most-frequently-played-songs)
			8. [Play song](#3128-play-song)
			9.  [Create invitation link](#3129-create-invitation-link)
			10. [Temporary join](#31210-temporary-join)
		    11. [Manage songs](#31211-manage-songs)
			    1. [Suggest song](#312111-suggest-song)
			    2. [Vote song](#312112-vote-song)
			    3. [Display playlist](#312113-display-playlist)
			    4. [Delete songs](#312114-delete-songs)
	2. [Usability](#32-usability)
		1. [Training period](#321-training-period)
		2. [Language](#322-language)
	3. [Reliability](#33-reliability)
		1. [Availability](#331-availability)
		2. [Mean Time Between Failures](#332-mean-time-between-failures)
		3. [Mean Time To Repair](#333-mean-time-to-repair)
	4. [Performance](#34-performance)
		1. [Response time](#341-response-time)
		2. [Capacity](#342-capacity)
	5. [Supportability](#35-supportability)
		1. [Conventions](#351-conventions)
	6. [Design Constraints](#36-design-constraints)
		1. [Git](#361-git)
		2. [YouTrack](#362-youtrack)
		3. [Database](#363-database)
	7. [Online User Documentation and Help System Requirements](#37-online-user-documentation-and-help-system-requirements)
	8. [Purchased Components](#38-purchased-components)
	9. [Interfaces](#39-interfaces)
		1. [User Interfaces](#391-user-interfaces)
		2. [Hardware Interfaces](#392-hardware-interfaces)
		3. [Software Interfaces](#393-software-interfaces)
		4. [Communications Interfaces](#394-communications-interfaces)
	10. [Licensing Requirements](#310-licensing-requirements)
	11. [Legal, Copyright, and Other Notices](#311-legal,-copyright,-and-other-notices)
	12. [Applicable Standards](#312-applicable-standards)
4. [Supporting Information](#4-supporting-information)
<!-- tocend -->

# 1. Introduction

## 1.1  Purpose

The purpose of this document is to present a detailed description of the PartyPlayer project. It will explain the purpose and features of the system, what the system will do and the constraints under which it must operate and how the system will react to external actions. This document is intended for both the stakeholders and the developers of the system.

## 1.2  Scope

This SRS applies to the entire PartyPlayer project. 'PartyPlayer' is a platform for sharing a playlist at parties where everybody can add songs to the playlist and can vote for a song. The song with the most votes gets played next. 
It will be realized as an Web-Application. The overview of the features and subsystems are documented in the Use-Case model shown in [Overall Description](#2-overall-description).

## 1.3  Definitions, Acronyms and Abbreviations

<dl>
  <dt>SRS</dt>
  <dd>Software Requirements Specification</dd>

</dl>

## 1.4  References

- [GitLab](https://gitlab.com/kzynn/partyplayer)
- [Use Case Diagram](https://gitlab.com/kzynn/partyplayer-doc/raw/master/ucd.png)
- [Blog](https://partyplayer.wordpress.com)
- [SAD](sad.md)

## 1.5  Overview

The following of this document gives overview over the functionality of the product and provides an more detailed description of the requirements.

# 2.  Overall Description

Our idea is to develop a music streaming and voting website for music at partys. The party organizer creates a PartyPlayer-group which the guests can join. He can predefine some songs or playlists for his party-playlist, but the guests can add songs too.

Here’s the best part, because every member of the group can now vote for the songs in the playlist and the song with the most votes will be played next. Furthermore the most loved songs and the recent played songs will be saved in a list. If there are no votes or the songs in the list are all played, PartyPlayer will play the most popular songs of the group.

In order to use the application one person has to log in with his spotify account to play songs. When theres enough time we may integrate the option to add songs from other sources (e.g. youtube link) to the playlist.

# 3. Specific Requirements

<div align="center"><img src="https://gitlab.com/kzynn/partyplayer-doc/raw/master/ucd.png" alt="PartyPlayer UCD" /></div>

## 3.1  Functionality

### 3.1.1 Account
#### 3.1.1.1 Create account

Any user can register providing username, password and email address.

[Use Case Documentation](/registerAccount/login.md)

#### 3.1.1.2 Login

The user can log in providing his username/email and password.

[Use Case Documentation](/UseCases/login.md)

#### 3.1.1.3 Logout

The user can log out.

#### 3.1.1.4 Add credentials for music platforms

The user can add his credentials for the supported music platforms.

<a href="/UseCases/addCredentials.md"> Use Case "add Credentials" Documentation </a>

#### 3.1.1.5 Delete account

The user can delete its account. All stored data about the user gets deleted in this case.

#### 3.1.1.6 Edit account

The user can edit its account like changing username or password.

#### 3.1.1.7 Confirm email

The user can confirm its provided email.

#### 3.1.1.8 Reset Password

When a user forgot its password he can request a new one providing its password email.

### 3.1.2 Event
#### 3.1.2.1 Create

A registered user can create an event.

#### 3.1.2.2 Add member

The event admin can add members to the event.

[Use Case Documentation](/UseCases/inviteUser.md)

#### 3.1.2.3 Remove member

The event admin can remove members from the event.

[Use Case Documentation](/UseCases/removeMembers.md)

#### 3.1.2.4 Leave

A event member can leave an event. He can not see what's happening in the event anymore.

[Use Case Documentation](/UseCases/leaveEvent.md)

#### 3.1.2.5 Delete

The event admin can delete the event. All data of the event is deleted.

#### 3.1.2.6 Rename

The event admin can rename the event and give it a name that does not already exist.

#### 3.1.2.7 Show most frequently played songs

All event members can view a list of the most frequently played songs.

#### 3.1.2.8 Play song

The owner of the group is the one who can play the songs in the queue on his device. The songs are played regardig which in the queue has received the most upvotes. The owner can start and stop the music playback.

#### 3.1.2.9 Create invitation link

The event admin can create an invitation link with that other unregistered users can join the event.

#### 3.1.2.10 Temporary join

An unregistered user can join an event with an invitation link. He then sets its username and can act as an normal user in that event.

#### 3.1.2.11 Manage songs
The following is a crud and combined to one use case:

[Use Case "crudEditPlaylist" Documentation](/UseCases/crudEditPlaylist.md")

##### 3.1.2.11.1 Suggest song

Any event member is able to add a song to the playlist.

##### 3.1.2.11.2 Vote song

Any event member can vote every suggested song up or down. A song with too many downvotes gets deleted from the list. The number of votes needed will depend on the amount of people in an event.

##### 3.1.2.11.3 Display playlist

Any event member can display the playlist and see how many upvotes each song has.

##### 3.1.2.11.4 Delete song

The event owner is able to delete a song from the playlist.

## 3.2  Usability

### 3.2.1 Training period

It should not require much time to understand or to be able to use PartyPlayer. Everybody at a party should be able to just jump in and use it without any training needed. Suggesting a song can take some time when you need to search for it. Voting the suggested songs is done in a few seconds. The Webapp is designed very clean so everybody should find what he's looking for very fast.

### 3.2.2 Language

The language of the Webapp will be english, so people around the world are able to understand and use our application.

## 3.3  Reliability

### 3.3.1 Availability

The application should be available around 90% of the time especially at the night. Downtimes during the night should not happen as most parties happen in the night. Availability beneath 90% is acceptable during development.

### 3.3.2 Mean Time Between Failures

Should be as high as possible. No estimation possible at this point.

### 3.3.3 Mean Time To Repair

Should be as low as possible. No estimation possible at this point.

## 3.4  Performance

### 3.4.1 Response time

As designed for parties there should not be much time between two songs. The load of the page, the joining, and the voting or adding of files should also not take much time. Response time for voting and playing the next song should be around 1 second on average. Registration, Login, Creation of a group or searching for a song to add can have larger response times with about 2 or 3 seconds on average.

### 3.4.2 Capacity

The first version of the application should be capable to manage a party with around 100 or 200 users or multiple smaller parties simultaneously. For productive usage, it should be capable of much more parties simultaneously.

## 3.5  Supportability

### 3.5.1 Conventions

The code in the backend and in the frontend should follow the standard conventions for Spring Boot or Angular. Both should be using speaking variable and function names.

## 3.6  Design Constraints

### 3.6.1 Git

Git is used for version control. You can find our repos on GitLab.

### 3.6.2 YouTrack

[YouTrack](http://youtrack.partyplayer.xyz) is used as issue tracker.

### 3.6.3 Database

For developing we will be using an H2 Database and at the server, we will use PostgreSQL.

## 3.7 Online User Documentation and Help System Requirements

No online help planned at the moment. The Webapplication should be intuitive and clear.

## 3.8  Purchased Components

n/a

## 3.9  Interfaces

### 3.9.1  User Interfaces

The user interface is the Web-portal. Functionality is described in Functionality.

### 3.9.2  Hardware Interfaces
n/A

### 3.9.3  Software Interfaces
n/a

### 3.9.4  Communications Interfaces

n/a

## 3.10 Licensing Requirements

n/a

## 3.11 Legal, Copyright, and Other Notices

n/a

## 3.12 Applicable Standards

n/a

# 4. Supporting Information
n/a
