## **PartyPlayer** <br/>Software Architecture Document 

## Table of Contents
1. [Introduction](#introduction)
    1. [Purpose](#purpose)
    2. [Scope](#scope)
    3. [Definitions, Acronyms and Abbreviations](#definitions-acronyms-and-abbreviations)
    4. [References](#references)
2. [Architectural Representation](#architectural-representation)
3. [Architectural Goals and Constraints](#architectural-goals-and-constraints)
4. [Use-Case View](#use-case-view)
    1. [Use-Case Realizations](#use-case-realizations)
5. [Logical View](#logical-view)
    1. [Overview](#overview)
    2. [Architecturally Significant Design Packages](#architecturally-significant-design-packages)
6. [Process View](#process-view)
7. [Deployment View](#deployment-view)
8. [Implementation View](#implementation-view)
    1. [Overview](#overview)
    2. [Layers](#layers)
9. [Data View](#data-view)
10. [Size and Performance](#size-and-performance)
11. [Quality](#quality)

## 1 Introduction
### 1.1 Purpose
In this article we want you to get a closer look on our project. In here you can find a detailed description of our 
project and how the two main components, frontend and backend, are interacting with each other. This means, that also 
the basic architecture is split up into two party, which make the project architecture a bit more complex to understand. 
### 1.2 Scope
Long story short, in here you find different views and architecture components of **PartyPlayer**, including classes, 
contoller and database.
### 1.3 Definitions, Acronyms and Abbreviations
| **Term** | **Definition** |
| -------- | -------------- |
| REST | **RE**presentational **S**tate **T**ransfer \[1\]
| API | **A**pplication **P**rogramming **I**nterface \[2\] |
| HTTP | Hyper Text Transfer Protocol
| n/a | not applicable |

### 1.4 References
\[1\] "REST", <https://www.dev-insider.de/konzept-aufbau-und-funktionsweise-von-rest-a-603152/>\
\[2\] "What is an API", <https://www.howtogeek.com/343877/what-is-an-api/>

## 2 Architectural Representation
![Overall Use Case Diagram](img/overallArchitecture/overall-tech.jpg)

## 3 Architectural Goals and Constraints
We decided to split our backend and frontend entirely. In order to connect them, we use a REST API, which is defined in
Swagger. The cool thing about Swagger is, that you can generate the API Server and Client with the push of a button in 
the most common languages. So
if we want a new frontend, we can just generate the API client, and implement the view. The point of generating the 
client is, that the API behaves like having native backend code.
For the backend, we use decided to use Spring Boot (Model and Controller) and as frontend we use
Angular (Model View Whatever).

[Our Api Specification (Swagger)](swagger-generate.yml)

## 4 Use-Case View
![Use Case Diagram](ucd.png)
### 4.1 Use-Case Realizations
* [Use Case - Credentials](https://gitlab.com/kzynn/partyplayer-doc/blob/master/UseCases/addCredentials.md)
* [Use Case - Playlist](https://gitlab.com/kzynn/partyplayer-doc/blob/master/UseCases/crudEditPlaylist.md)
* [Use Case - Invite User](https://gitlab.com/kzynn/partyplayer-doc/blob/master/UseCases/inviteUser.md)
* [Use Case - Login](https://gitlab.com/kzynn/partyplayer-doc/blob/master/UseCases/login.md)
* [Use Case - Register](https://gitlab.com/kzynn/partyplayer-doc/blob/master/UseCases/registerAccount.md)

## 5 Logical View
**Backend Logical View:**
![Backend Logical View](img/spring_abstract.png)

## 5.2 Architecturally Significant Design Packages
![Package Structure Backend](img/packages.PNG)
<br/>
Here you can see the package structure of our backend. Note that it has seemingly two model packages. This 
is not the case. The models in the api.model package are for API requests and responses only. In the api package are
the controllers, including the api interfaces.
![ClassDiagram](img/ClassDiagramMVC.jpg)

## 6 Process View
n/a

## 7 Deployment View
![Deployment View](img/deploy+tech.png)

## 8 Implementation View
n/a

## 9 Data View
![Database Model](img/DatabaseDiagram.jpg)

## 10 Size and Performance
n/a

## 11 Quality/Metrics
n/a
